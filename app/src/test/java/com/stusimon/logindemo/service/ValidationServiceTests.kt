package com.stusimon.logindemo.service

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.threeten.bp.LocalDate
import java.util.*

class ValidationServiceTests {

    private val validationService = ValidationService()

    // currently some email addresses that would not be valid are expected to be valid
    // this shows that the validation functionality could still be improved in the future
    @ParameterizedTest
    @CsvSource(
        "a, false",
        "a.bc, false",
        "a@b.c, false",
        "@b.com, false",
        "abc@.com, false",
        "abc@, false",
        "abc@abc, false",
        "abc@abc., false",
        "abc@abc.c, false",
        "muster@test@be.ch, false",
        "muster@.ch, false",
        "muster@testbe.@ch, false",
        "muster_test.be.ch, false",
        "a@b.ch, true",
        "a@_.ch, true",
        "a@_.4ch, true",
        "muster_test@be.ch, true",
        "muster_test@be.ch-, true",
        "mus.ter@test.ch, true",
        "0mus_ter@test.ch, true",
        "-mus_ter@test.ch0, true",
        "-mus&%çter@test.ch0, true",
    )
    fun validateEmail_WhenEmailIsValid_ReturnsTrue(email: String, expected: Boolean){
        // Act
        var result = validationService.validateEmail(email)

        // Assert
        assertEquals(expected, result)
    }

    @Test
    fun validateEmail_WhenEmailIsEmpty_ReturnsFalse(){
        // Act
        var result = ValidationService().validateEmail("")

        // Assert
        assertEquals(false, result)
    }

    @ParameterizedTest
    @CsvSource(
        "1899, false",
        "0, false",
        "-1, false",
        "-1999, false",
        "2020, false",
        "1900, true",
        "2019, true",
        "2000, true",
    )
    fun validateBirthYear_ChecksIfBirthdayIsInRange(birthYear: Int, expected: Boolean) {
        // Arrange
        val birthdayEpochDay = LocalDate.of(birthYear, 1, 1).toEpochDay()

        // Act
        var result = validationService.validateBirthday(birthdayEpochDay)

        // Assert
        assertEquals(expected, result)
    }

    @ParameterizedTest
    @CsvSource(
        "A, true",
        "Test, true",
        "Test Tester, true",

    )
    fun validateName_ChecksIfNameHasAtLeastOneCharacter(name: String, expected: Boolean) {
        // Act
        var result = validationService.validateName(name)

        // Assert
        assertEquals(expected, result)
    }

    @Test
    fun validateName_WhenNameIsEmpty_ReturnsFalse(){
        // Act
        var result = ValidationService().validateName("")

        // Assert
        assertEquals(false, result)
    }
}