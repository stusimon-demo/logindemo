package com.stusimon.logindemo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.stusimon.logindemo.model.UserInfo
import com.stusimon.logindemo.service.IValidationService
import org.koin.core.KoinComponent
import org.koin.core.inject

class LoginViewModel() : ViewModel(), KoinComponent {
    private val mutableLoginState = MutableLiveData<BindableLoginState>()
    val loginState: LiveData<BindableLoginState> = mutableLoginState

    private val validationService : IValidationService by inject()

    fun validate(userInfo: UserInfo): Boolean {
        /*
            it would also be possible to validate each entry after it was entered instead of
            all together in the end. This might be a better user-experience as the user is not
            confronted with multiple errors at the end
        */
        return validateAndUpdateUI(userInfo)
    }

    private fun validateAndUpdateUI(userInfo: UserInfo): Boolean{
        // Name shows that this Method does multiple things. Should be refactored, such that the methods are split in two
        // --> Single Responsibility Pattern.
        val isNameValid = validationService.validateName(userInfo.name)
        val isEmailValid = validationService.validateEmail(userInfo.email)
        val isBirthday = validationService.validateBirthday(userInfo.birthdayEpochDay)

        // maybe introduce a new data-type which combines the arguments, so that not three parameters are needed for the method
        mutableLoginState.value = BindableLoginState.createFromLoginResult(isNameValid, isEmailValid, isBirthday)

        return isNameValid && isEmailValid && isBirthday
    }
}