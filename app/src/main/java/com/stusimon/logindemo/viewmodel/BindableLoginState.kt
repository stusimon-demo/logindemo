package com.stusimon.logindemo.viewmodel

import com.stusimon.logindemo.R

class BindableLoginState(
    val nameError: Int?,
    val emailError: Int?,
    val birthdayError: Int?
) {

    companion object{
        fun createFromLoginResult(isNameValid: Boolean,
                                  isEmailValid: Boolean,
                                  isBirthdayValid: Boolean): BindableLoginState{

            var nameError: Int? = null
            var emailError: Int? = null
            var birthdayError: Int? = null

            if (!isNameValid)
                nameError = R.string.name_not_valid
            if (!isEmailValid)
                emailError = R.string.email_not_valid
            if (!isBirthdayValid)
                birthdayError = R.string.birthday_not_valid

            return BindableLoginState(nameError, emailError, birthdayError)
        }
    }

}

