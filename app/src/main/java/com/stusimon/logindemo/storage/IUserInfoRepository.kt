package com.stusimon.logindemo.storage

import android.content.Context
import com.stusimon.logindemo.model.UserInfo

interface IUserInfoRepository {
    fun storeUserInfo(userInfo: UserInfo, context: Context)

    fun readUserInfo(context: Context): UserInfo
}