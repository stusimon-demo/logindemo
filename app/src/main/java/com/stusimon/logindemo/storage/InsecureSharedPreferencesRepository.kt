package com.stusimon.logindemo.storage

import android.content.Context
import com.stusimon.logindemo.R
import com.stusimon.logindemo.model.UserInfo

/**
 * The data that is stored here is personally identifiable information and should therefore be
 * protected accordingly. Storing it unencrypted in the shared preferences is not secure.
 *
 * This has to be changed in the future. But first we should clarify why this data needs to be stored
 * at all, as it is usually better to get the data from the backend.
 *
 * If the data really needs to be stored it could be stored in an encrypted DB (e.g. use Room
 * with SQLCipher), but this raises the question, how the encryption key is generated and stored
 *
 * The most secure way would be to let the key be generated in the hardwarebacked keystore. The key
 * should then never leave this 'secure-enclave' but it can be used to encrypt/decrypt the key which
 * is used to encrypt the DB. This might require that the user enters some password or authenticates
 * with biometrics
 */
class InsecureSharedPreferencesRepository: IUserInfoRepository {
    override fun storeUserInfo(userInfo: UserInfo, context: Context) {
        val sharedPreferences = context.getSharedPreferences(R.string.shared_preferences_key.toString(), Context.MODE_PRIVATE)
        with (sharedPreferences.edit()){
            putString(R.string.name_storage_key.toString(), userInfo.name)
            putString(R.string.email_storage_key.toString(), userInfo.email)
            putLong(R.string.birthday_storage_key.toString(), userInfo.birthdayEpochDay)
            apply()
        }
    }

    override fun readUserInfo(context: Context): UserInfo {
        val sharedPreferences = context.getSharedPreferences(R.string.shared_preferences_key.toString(), Context.MODE_PRIVATE)
        var name = sharedPreferences.getString(R.string.name_storage_key.toString(), "")
        var email = sharedPreferences.getString(R.string.email_storage_key.toString(), "")
        val birthday = sharedPreferences.getLong(R.string.birthday_storage_key.toString(), Long.MIN_VALUE)

        if (name == null){
            name = ""
        }

        if (email == null){
            email = ""
        }

        return UserInfo(name, email, birthday)
    }


}