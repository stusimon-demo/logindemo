package com.stusimon.logindemo.ui

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.Observer
import com.stusimon.logindemo.R
import com.stusimon.logindemo.model.UserInfo
import com.stusimon.logindemo.storage.IUserInfoRepository
import com.stusimon.logindemo.viewmodel.LoginViewModel
import com.stusimon.logindemo.viewmodel.LoginViewModelFactory
import org.koin.android.ext.android.inject
import org.koin.core.inject
import org.threeten.bp.LocalDate

class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel : LoginViewModel
    private lateinit var nameEditText: EditText
    private lateinit var emailEditText: EditText
    private lateinit var dateEditText: EditText
    private lateinit var confirmButton: Button

    private var pickedBirthdayEpochDay: Long = Long.MIN_VALUE
    private val USERINFO_EXTRA_KEY = "userinfo-extra"

    private val userInfoRepository : IUserInfoRepository by inject()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        nameEditText = findViewById(R.id.editTextName)
        emailEditText = findViewById(R.id.editTextEmail)
        dateEditText = findViewById(R.id.editTextDate)
        confirmButton = findViewById(R.id.confirmButton)
        setupDatePicker()
        setInitialTexts()

        loginViewModel = ViewModelProvider(this, LoginViewModelFactory())
            .get(LoginViewModel::class.java)

        confirmButton.setOnClickListener{
            val name = nameEditText.text.toString()
            val email = emailEditText.text.toString()
            val userInfo = UserInfo(name, email, pickedBirthdayEpochDay)
            if (loginViewModel.validate(userInfo)){
                userInfoRepository.storeUserInfo(userInfo, this)
                showConfirmation(userInfo)
            }
        }

        loginViewModel.loginState.observe(this@LoginActivity, Observer { state ->
            val loginState = state ?: return@Observer
            if(loginState.nameError != null){
                nameEditText.error = getString(loginState.nameError)
            }
            if(loginState.emailError != null){
                emailEditText.error = getString(loginState.emailError)
            }
            // this is done as a workaround because the display of the error does not work with the
            // birthdayEditText as the error text is only displayed when the editText is in focus
            // which is disabled in order to launch the picker on the first tap
            if(loginState.birthdayError != null){
                Toast.makeText(this, getString(loginState.birthdayError), Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun showConfirmation(userInfo: UserInfo){
        val confirmationIntent = Intent(this, ConfirmationActivity::class.java)
        confirmationIntent.putExtra(USERINFO_EXTRA_KEY, userInfo)

        startActivity(confirmationIntent)
    }

    private fun setInitialTexts(){
        nameEditText.hint = getString(R.string.name_hint)
        emailEditText.hint = getString(R.string.email_hint)
        dateEditText.hint = getString(R.string.date_hint)
        confirmButton.text = getString(R.string.confirm_button_text)
    }

    private fun setupDatePicker(){
        dateEditText.isFocusableInTouchMode = false
        dateEditText.setOnClickListener() {
            var selectedDate = getSelectedDate()
            val year = selectedDate.year
            val month = selectedDate.monthValue -1 // due to picker counting january as 0
            val day = selectedDate.dayOfMonth

            var picker = DatePickerDialog(this, R.style.MySpinnerDatePickerStyle, DatePickerDialog.OnDateSetListener{
                    _, year, month, day ->
                val pickedDate = LocalDate.of(year, month+1, day)
                pickedBirthdayEpochDay = pickedDate.toEpochDay()

                dateEditText.setText("$day.${month+1}.$year")

            }, year, month, day)
            picker.show()
        }
    }

    private fun getSelectedDate(): LocalDate {
        return if (pickedBirthdayEpochDay == Long.MIN_VALUE){
            LocalDate.now()
        } else {
            LocalDate.ofEpochDay(pickedBirthdayEpochDay)
        }
    }
}