package com.stusimon.logindemo.ui

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.stusimon.logindemo.R
import com.stusimon.logindemo.model.UserInfo
import org.threeten.bp.LocalDate

class ConfirmationActivity : AppCompatActivity() {
    private val USERINFO_EXTRA_KEY = "userinfo-extra"

    private lateinit var nameTextView: TextView
    private lateinit var emailTextView: TextView
    private lateinit var birthdayTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)

        nameTextView = findViewById(R.id.nameTextView)
        emailTextView = findViewById(R.id.emailTextView)
        birthdayTextView = findViewById(R.id.birthdayTextView)

        val userInfo = intent.getParcelableExtra<UserInfo>(USERINFO_EXTRA_KEY)!!
        updateTextViews(userInfo)
    }

    private fun updateTextViews(userInfo: UserInfo){
        nameTextView.text = userInfo.name
        emailTextView.text = userInfo.email

        val date = LocalDate.ofEpochDay(userInfo.birthdayEpochDay)
        birthdayTextView.text = "${date.dayOfMonth}.${date.monthValue}.${date.year}"
    }
}