package com.stusimon.logindemo.di

import com.stusimon.logindemo.service.IValidationService
import com.stusimon.logindemo.service.ValidationService
import com.stusimon.logindemo.storage.IUserInfoRepository
import com.stusimon.logindemo.storage.InsecureSharedPreferencesRepository
import org.koin.dsl.module

val appDependencies = module {
    single<IValidationService> {ValidationService()}
    single<IUserInfoRepository> {InsecureSharedPreferencesRepository()}
}