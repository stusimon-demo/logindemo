package com.stusimon.logindemo.service

import org.threeten.bp.LocalDate

class ValidationService : IValidationService {


    override fun validateName(name: String): Boolean {
        return name != null && name.isNotEmpty()
    }

    override fun validateEmail(email: String): Boolean {
        if (email == null || email == "")
            return false

        val parts = email.split("@")

        if(parts == null || parts.count() != 2) return false

        val isFirstPartValid = validateFirstPartOfEmail(parts[0])
        val isSecondPartValid = validateSecondPartOfEmail(parts[1])

        return isFirstPartValid && isSecondPartValid;
    }

    private fun validateFirstPartOfEmail(firstPart: String): Boolean{
        return firstPart.isNotEmpty()
    }

    private fun validateSecondPartOfEmail(secondPart: String): Boolean{
        val parts = secondPart.split(".")
        val lastPart = parts.last()

        if (lastPart.length < 2)
            return false

        if (parts.count() < 2 || (parts.count() == 2 && parts[0].isEmpty()))
            return false

        return true
    }

    override fun validateBirthday(birthdayEpochDay: Long): Boolean {
        val birthYear = LocalDate.ofEpochDay(birthdayEpochDay).year

        return birthYear in 1900..2019
    }

}