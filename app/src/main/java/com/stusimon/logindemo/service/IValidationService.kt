package com.stusimon.logindemo.service

/**
 * This service provides the functionality to validate the user entered data according to predefined rules
 *
 * Note:
 * As the data is entered by the user we should also perform input validation and data sanitization
 * to prevent attacks like SQL injections, Buffer Overflow etc.
 *
 * This has not been implemented yet but there is a story: LD-8
 */
interface IValidationService {
    fun validateName(name: String): Boolean
    fun validateEmail(email: String): Boolean
    fun validateBirthday(birthYear: Long): Boolean
}