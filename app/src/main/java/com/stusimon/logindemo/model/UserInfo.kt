package com.stusimon.logindemo.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserInfo(
    val name: String = "",
    val email: String = "",
    val birthdayEpochDay: Long = Long.MIN_VALUE
) : Parcelable