package com.stusimon.logindemo

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import com.stusimon.logindemo.di.appDependencies
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class LoginDemo: Application() {
    override fun onCreate() {
        super.onCreate()

        AndroidThreeTen.init(this);

        startKoin{
            androidContext(this@LoginDemo)
            modules(appDependencies)
        }
    }
}